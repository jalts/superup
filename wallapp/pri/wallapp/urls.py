from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('register', views.register),
    path('login', views.login),
    path('main_page', views.main_page),
    path('logout', views.logout),
    path('create_message', views.add_message),
    path('profile/<int:id>', views.profile),
    path('edit/<int:id>', views.edit_mess),
    path('delete/<int:id>', views.delete_mess),
]