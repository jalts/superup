from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import *
import bcrypt

def index(request):
    # return HttpResponse("you did")
    return render(request, "index.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashed_pw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()
            print(hashed_pw)
            user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=hashed_pw)
            #request.session['name'] = new_user.first_name
            request.session['user_id'] = user.id
            return redirect('/main_page')
    return redirect('/')

def login(request):
    user = User.objects.filter(email=request.POST['email'])
    if len(user) > 0:
        user = user[0]
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            #request.session['name'] = logged_user.first_name
            return redirect('/main_page')
    messages.error(request, "email or pass is incorrect")
    return redirect('/')

def main_page(request):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    context = {
        'user': User.objects.get(id=request.session['user_id']),
        'all_messages': Wall_Message.objects.all(),
    }
    return render(request, "main_page.html", context)

def logout(request):
    request.session.clear()
    return redirect('/')

# wall functionality

def add_message(request):
    if request.method == "POST":
        errors = Wall_Message.objects.validator(request.POST)
        if errors:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/main_page')
        # is this request session correct?
        new_mess = Wall_Message.objects.create(content=request.POST['content'], poster=User.objects.get(id=request.session['user_id']))
        return redirect('/main_page')
    return redirect('/')

def profile(request, id):
    context = {
        'one_user': User.objects.get(id=id)
    }
    return render(request, 'profile.html', context)

def edit_mess(request, id):
    one_mess = Wall_Message.objects.get(id=id)
    if request.method == 'POST':
        one_mess.content = request.POST['content']
        one_mess.save()
        return redirect(f'/profile/{str(one_mess.poster.id)}')
    context = {
        'edit_mess': one_mess
    }
    return render(request, 'edit.html', context)

def delete_mess(request, id):
    Wall_Message.objects.get(id=id).delete()
    return redirect('/main_page')