from __future__ import unicode_literals
from django.db import models
import re

class UserManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if len(reqPOST['first_name']) < 2:
            errors['first_name'] = "first name must be at least 2 chars"
        if len(reqPOST['last_name']) < 2:
            errors['last_name'] = "first name must be at least 2 chars"
        if not EMAIL_REGEX.match(reqPOST['email']):
            errors['email'] = "email must be valid format"
        if len(reqPOST['password']) < 6:
            errors['password'] = 'password must be at least 8 chars'
        if reqPOST['password'] != reqPOST['confirm_password']:
            errors['confirm_password'] = "passwords do not match"
        return errors


class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()


class MessageManager(models.Manager):
    def validator(self, form):
        errors = {}
        if len(form['content']) < 3:
            errors['length'] = "message must be at least 3 chars!"
        return errors

class Wall_Message(models.Model):
    poster = models.ForeignKey(User, related_name="messages", on_delete=models.CASCADE)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = MessageManager()

