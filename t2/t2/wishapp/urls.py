from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('register', views.register),
    path('login', views.login),
    path('success', views.success),
    path('logout', views.logout),
    path('wish/remove/<int:id>', views.remove_wish),
    path('wishes/grant/<int:id>', views.grant_wish),
    path('create_page', views.create_a_wish),
    path('wishes/create', views.create_wish),
]