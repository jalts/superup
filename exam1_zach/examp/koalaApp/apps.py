from django.apps import AppConfig


class KoalaappConfig(AppConfig):
    name = 'koalaApp'
