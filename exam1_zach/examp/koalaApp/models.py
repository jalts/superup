from __future__ import unicode_literals
from django.db import models
import re

class UserManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if len(reqPOST['first_name']) < 2:
            errors['first_name'] = "first name must be at least 2 chars"
        if len(reqPOST['last_name']) < 2:
            errors['last_name'] = "first name must be at least 2 chars"
        if not EMAIL_REGEX.match(reqPOST['email']):
            errors['email'] = "email must be valid format" 
        if len(reqPOST['password']) < 6:
            errors['password'] = 'password must be at least 8 chars'
        if reqPOST['password'] != reqPOST['confirm_password']:
            errors['confirm_password'] = "passwords do not match"
        return errors


class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

class KoalaManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        if len(reqPOST['koala_name']) < 2:
            errors['koala_name'] = "koala name is too short"
        if len(reqPOST['talent']) < 2:
            errors['talent'] = "talent is too short"
        koalas_with_same_name = Koala.objects.filter(name=reqPOST['koala_name'])
        if len(koalas_with_same_name) > 0:
            errors['duplicate'] = "That name is already taken!"
        return errors

class Koala(models.Model):
    name = models.CharField(max_length=40)
    talent = models.TextField()
    user = models.ForeignKey(User, related_name="koalas_owned", on_delete=models.CASCADE)
    users_votes = models.ManyToManyField(User, related_name="voted_koalas")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = KoalaManager()



