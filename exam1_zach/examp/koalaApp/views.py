from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import User, Koala
import bcrypt
from django.db.models import Count

def index(request):
    # return HttpResponse("you did")
    return render(request, "index.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashed_pw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()
            print(hashed_pw)
            user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=hashed_pw)
            request.session['user_id'] = user.id
            return redirect('/main_page')
    return redirect('/')

def login(request):
    user = User.objects.filter(email=request.POST['email'])
    if len(user) > 0:
        user = user[0]
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            return redirect('/main_page')
    messages.error(request, "email or pass is incorrect")
    return redirect('/')

def main_page(request):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    context = {
        'user': User.objects.get(id=request.session['user_id']),
        'all_koalas': Koala.objects.all()
    }
    return render(request, "main_page.html", context)

def logout(request):
    request.session.clear()
    return redirect('/')

def create_koala(request):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        errors = Koala.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/main_page')
        else:
            koala = Koala.objects.create(name=request.POST['koala_name'], talent=request.POST['talent'], user=User.objects.get(id=request.session['user_id']))
        return redirect('/main_page')
    return redirect('/main_page')

def profile(request):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    context = {
        'user': User.objects.get(id=request.session['user_id'])
    }
    return render(request, "profile.html", context)
    
def show_koala(request, id):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    koala_with_id = Koala.objects.filter(id=id)
    if len(koala_with_id) > 0:
        context = {
            'koala': Koala.objects.get(id=id)
        }
        return render(request, "one_koala.html", context)
    else:
        messages.error(request, "koala not found.")
        return redirect('/user')

def destroy_koala(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        koala_with_id = Koala.objects.filter(id=id)
        if len(koala_with_id) > 0:
            koala = koala_with_id[0]
            if koala.user.id == request.session['user_id']:
                koala.delete()
    return redirect('/main_page')

def voting_page(request):
    if 'user_id' not in request.session:
        return redirect('/')
    context = {
        #"all_koalas": Koala.objects.all(),
        "all_koalas": Koala.objects.annotate(votes=Count('users_votes')).order_by('-votes'),
        'user': User.objects.get(id=request.session['user_id'])
    }
    return render(request, "voting_page.html", context)

def vote_koala(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        koala_with_id = Koala.objects.filter(id=id)
        if len(koala_with_id) > 0:
            koala = Koala.objects.get(id=id)
            user = User.objects.get(id=request.session['user_id'])
            koala.users_votes.add(user)
            #user.voted_koalas.add(koala)
    return redirect('/voting')

def unvote_koala(request, id):
    if 'user_id' not in request.session:  #checking to see if someone is really using the application
        return redirect('/')
    if request.method == "POST":  #checking to see if this is a POST method 
        koala_with_id = Koala.objects.filter(id=id)  #checking to make sure we are filtering within our app - is the ID ok?
        if len(koala_with_id) > 0:  
            koala = Koala.objects.get(id=id)  #grabbing the koala in question
            user = User.objects.get(id=request.session['user_id'])  #grabbing the user
            koala.users_votes.remove(user)  # instead of add, we are using the remove function
            #user.voted_koalas.add(koala)
    return redirect('/voting')


    
